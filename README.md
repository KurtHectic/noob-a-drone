# README #

Noob-a-Drone project, based on DroneKit-Android

* Al momento è una implementazione di quanto descritto in [questo tutorial](http://android.dronekit.io/first_app.html)

* Per connettere un drone simulato installare SITL come descritto [qui](http://ardupilot.org/dev/docs/setting-up-sitl-on-linux.html)

* Per simulare un drone che possa connettersi ad un cellulare spostarsi nella cartella `/ardupilot/ArduCopter`

* Far partire il simulatore con: `sim_vehicle.py -L 3DRBerkeley --console --map  --out <your_phone_ip>:14550`

* Per trovare l'ip del cellulare android: impostazioni > wifi > menu in alto a dx > avanzate
